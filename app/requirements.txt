django==3.2.4
gunicorn==20.1.0
humanhash3==0.0.6
django-qr-code==2.2.0
