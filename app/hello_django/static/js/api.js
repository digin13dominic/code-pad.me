var assemblySource="\
section	.text\n\
    global _start\n\
\n\
_start:\n\
\n\
    xor	eax, eax\n\
    lea	edx, [rax+len]\n\
    mov	al, 1\n\
    mov	esi, msg\n\
    mov	edi, eax\n\
    syscall\n\
\n\
    xor	edi, edi\n\
    lea	eax, [rdi+60]\n\
    syscall\n\
\n\
section	.rodata\n\
\n\
msg	db 'hello, code-pad.me', 0xa\n\
len	equ	$ - msg\n\
";var bashSource="echo \"hello, code-pad.me\"";var basicSource="PRINT \"hello, code-pad.me\"";var cSource="\
// Powered by Code-Pad.me\n\
#include <stdio.h>\n\
\n\
int main(void) {\n\
    printf(\"Hello Code-Pad.me!\\n\");\n\
    return 0;\n\
}\n\
";var csharpSource="\
public class Hello {\n\
    public static void Main() {\n\
        System.Console.WriteLine(\"hello, code-pad.me\");\n\
    }\n\
}\n\
";var cppSource="\
#include <iostream>\n\
\n\
int main() {\n\
    std::cout << \"hello, code-pad.me\" << std::endl;\n\
    return 0;\n\
}\n\
";var clojureSource="(println \"hello, code-pad.me\")\n";var cobolSource="\
IDENTIFICATION DIVISION.\n\
PROGRAM-ID. MAIN.\n\
PROCEDURE DIVISION.\n\
DISPLAY \"hello, code-pad.me\".\n\
STOP RUN.\n\
";var lispSource="(write-line \"hello, code-pad.me\")";var dSource="\
import std.stdio;\n\
\n\
void main()\n\
{\n\
    writeln(\"hello, code-pad.me\");\n\
}\n\
";var elixirSource="IO.puts \"hello, code-pad.me\"";var erlangSource="\
main(_) ->\n\
    io:fwrite(\"hello, code-pad.me\\n\").\n\
";var executableSource="\
Code-Pad.me IDE assumes that content of executable is Base64 encoded.\n\
\n\
This means that you should Base64 encode content of your binary,\n\
paste it here and click \"Run\".\n\
\n\
Here is an example of compiled \"hello, code-pad.me\" NASM program.\n\
Content of compiled binary is Base64 encoded and used as source code.\n\
\n\
https://api.Code-Pad.me/?kS_f\n\
";var fsharpSource="printfn \"hello, code-pad.me\"\n";var fortranSource="\
program main\n\
    print *, \"hello, code-pad.me\"\n\
end\n\
";var goSource="\
package main\n\
\n\
import \"fmt\"\n\
\n\
func main() {\n\
    fmt.Println(\"hello, code-pad.me\")\n\
}\n\
";var groovySource="println \"hello, code-pad.me\"\n";var haskellSource="main = putStrLn \"hello, code-pad.me\"";var javaSource="\
public class Main {\n\
    public static void main(String[] args) {\n\
        System.out.println(\"hello, code-pad.me\");\n\
    }\n\
}\n\
";var javaScriptSource="console.log(\"hello, code-pad.me\");";var kotlinSource="\
fun main() {\n\
    println(\"hello, code-pad.me\")\n\
}\n\
";var luaSource="print(\"hello, code-pad.me\")";var objectiveCSource="\
#import <Foundation/Foundation.h>\n\
\n\
int main() {\n\
    @autoreleasepool {\n\
        char name[10];\n\
        scanf(\"%s\", name);\n\
        NSString *message = [NSString stringWithFormat:@\"hello, %s\\n\", name];\n\
        printf(\"%s\", message.UTF8String);\n\
    }\n\
    return 0;\n\
}\n\
";var ocamlSource="print_endline \"hello, code-pad.me\"";var octaveSource="printf(\"hello, code-pad.me\\n\");";var pascalSource="\
program Hello;\n\
begin\n\
    writeln ('hello, code-pad.me')\n\
end.\n\
";var perlSource="\
my $name = <STDIN>;\n\
print \"hello, $name\";\n\
";var phpSource="\
<?php\n\
print(\"hello, code-pad.me\\n\");\n\
?>\n\
";var plainTextSource="hello, code-pad.me\n";var prologSource="\
:- initialization(main).\n\
main :- write('hello, code-pad.me\\n').\n\
";var pythonSource="print(\"hello, code-pad.me\")";var rSource="cat(\"hello, code-pad.me\\n\")";var rubySource="puts \"hello, code-pad.me\"";var rustSource="\
fn main() {\n\
    println!(\"hello, code-pad.me\");\n\
}\n\
";var scalaSource="\
object Main {\n\
    def main(args: Array[String]) = {\n\
        val name = scala.io.StdIn.readLine()\n\
        println(\"hello, \"+ name)\n\
    }\n\
}\n\
";var sqliteSource="\
-- On Code-Pad.me IDE your SQL script is run on chinook database (https://www.sqlitetutorial.net/sqlite-sample-database).\n\
-- For more information about how to use SQL with Code-Pad.me API please\n\
-- watch this asciicast: https://asciinema.org/a/326975.\n\
SELECT\n\
    Name, COUNT(*) AS num_albums\n\
FROM artists JOIN albums\n\
ON albums.ArtistID = artists.ArtistID\n\
GROUP BY Name\n\
ORDER BY num_albums DESC\n\
LIMIT 4;\n\
";var sqliteAdditionalFiles="";var swiftSource="\
import Foundation\n\
let name = readLine()\n\
print(\"hello, \\(name!)\")\n\
";var typescriptSource="console.log(\"hello, code-pad.me\");";var vbSource="\
Public Module Program\n\
   Public Sub Main()\n\
      Console.WriteLine(\"hello, code-pad.me\")\n\
   End Sub\n\
End Module\n\
";var c3Source="\
// On the Code-Pad.me IDE, C3 is automatically\n\
// updated every hour to the latest commit on master branch.\n\
module main;\n\
\n\
extern func void printf(char *str, ...);\n\
\n\
func int main()\n\
{\n\
    printf(\"hello, code-pad.me\\n\");\n\
    return 0;\n\
}\n\
";var javaTestSource="\
import static org.junit.jupiter.api.Assertions.assertEquals;\n\
\n\
import org.junit.jupiter.api.Test;\n\
\n\
class MainTest {\n\
    static class Calculator {\n\
        public int add(int x, int y) {\n\
            return x + y;\n\
        }\n\
    }\n\
\n\
    private final Calculator calculator = new Calculator();\n\
\n\
    @Test\n\
    void addition() {\n\
        assertEquals(2, calculator.add(1, 1));\n\
    }\n\
}\n\
";var mpiccSource="\
// Try adding \"-n 5\" (without quotes) into command line arguments. \n\
#include <mpi.h>\n\
\n\
#include <stdio.h>\n\
\n\
int main()\n\
{\n\
    MPI_Init(NULL, NULL);\n\
\n\
    int world_size;\n\
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);\n\
\n\
    int world_rank;\n\
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);\n\
\n\
    printf(\"Hello from processor with rank %d out of %d processors.\\n\", world_rank, world_size);\n\
\n\
    MPI_Finalize();\n\
\n\
    return 0;\n\
}\n\
";var mpicxxSource="\
// Try adding \"-n 5\" (without quotes) into command line arguments. \n\
#include <mpi.h>\n\
\n\
#include <iostream>\n\
\n\
int main()\n\
{\n\
    MPI_Init(NULL, NULL);\n\
\n\
    int world_size;\n\
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);\n\
\n\
    int world_rank;\n\
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);\n\
\n\
    std::cout << \"Hello from processor with rank \"\n\
              << world_rank << \" out of \" << world_size << \" processors.\\n\";\n\
\n\
    MPI_Finalize();\n\
\n\
    return 0;\n\
}\n\
";var mpipySource="\
# Try adding \"-n 5\" (without quotes) into command line arguments. \n\
from mpi4py import MPI\n\
\n\
comm = MPI.COMM_WORLD\n\
world_size = comm.Get_size()\n\
world_rank = comm.Get_rank()\n\
\n\
print(f\"Hello from processor with rank {world_rank} out of {world_size} processors\")\n\
";var nimSource="\
# On the Code-Pad.me IDE, Nim is automatically\n\
# updated every day to the latest stable version.\n\
echo \"hello, code-pad.me\"\n\
";var pythonForMlSource="\
import mlxtend\n\
import numpy\n\
import pandas\n\
import scipy\n\
import sklearn\n\
\n\
print(\"hello, code-pad.me\")\n\
";var bosqueSource="\
// On the Code-Pad.me IDE, Bosque (https://github.com/microsoft/BosqueLanguage)\n\
// is automatically updated every hour to the latest commit on master branch.\n\
\n\
namespace NSMain;\n\
\n\
concept WithName {\n\
    invariant $name != \"\";\n\
\n\
    field name: String;\n\
}\n\
\n\
concept Greeting {\n\
    abstract method sayHello(): String;\n\
    \n\
    virtual method sayGoodbye(): String {\n\
        return \"goodbye\";\n\
    }\n\
}\n\
\n\
entity GenericGreeting provides Greeting {\n\
    const instance: GenericGreeting = GenericGreeting@{};\n\
\n\
    override method sayHello(): String {\n\
        return \"hello code-pad.me\";\n\
    }\n\
}\n\
\n\
entity NamedGreeting provides WithName, Greeting {\n\
    override method sayHello(): String {\n\
        return String::concat(\"hello\", \" \", this.name);\n\
    }\n\
}\n\
\n\
entrypoint function main(arg?: String): String {\n\
    var val = arg ?| \"\";\n\
    if (val == \"1\") {\n\
        return GenericGreeting@{}.sayHello();\n\
    }\n\
    elif (val == \"2\") {\n\
        return GenericGreeting::instance.sayHello();\n\
    }\n\
    else {\n\
        return NamedGreeting@{name=\"bob\"}.sayHello();\n\
    }\n\
}\n\
";var cppTestSource="\
#include <gtest/gtest.h>\n\
\n\
int add(int x, int y) {\n\
    return x + y;\n\
}\n\
\n\
TEST(AdditionTest, NeutralElement) {\n\
    EXPECT_EQ(1, add(1, 0));\n\
    EXPECT_EQ(1, add(0, 1));\n\
    EXPECT_EQ(0, add(0, 0));\n\
}\n\
\n\
TEST(AdditionTest, CommutativeProperty) {\n\
    EXPECT_EQ(add(2, 3), add(3, 2));\n\
}\n\
\n\
int main(int argc, char **argv) {\n\
    ::testing::InitGoogleTest(&argc, argv);\n\
    return RUN_ALL_TESTS();\n\
}\n\
";var csharpTestSource="\
using NUnit.Framework;\n\
\n\
public class Calculator\n\
{\n\
    public int add(int a, int b)\n\
    {\n\
        return a + b;\n\
    }\n\
}\n\
\n\
[TestFixture]\n\
public class Tests\n\
{\n\
    private Calculator calculator;\n\
\n\
    [SetUp]\n\
    protected void SetUp()\n\
    {\n\
        calculator = new Calculator();\n\
    }\n\
\n\
    [Test]\n\
    public void NeutralElement()\n\
    {\n\
        Assert.AreEqual(1, calculator.add(1, 0));\n\
        Assert.AreEqual(1, calculator.add(0, 1));\n\
        Assert.AreEqual(0, calculator.add(0, 0));\n\
    }\n\
\n\
    [Test]\n\
    public void CommutativeProperty()\n\
    {\n\
        Assert.AreEqual(calculator.add(2, 3), calculator.add(3, 2));\n\
    }\n\
}\n\
";var sources={45:assemblySource,46:bashSource,47:basicSource,48:cSource,49:cSource,50:cSource,51:csharpSource,52:cppSource,53:cppSource,54:cppSource,55:lispSource,56:dSource,57:elixirSource,58:erlangSource,44:executableSource,59:fortranSource,60:goSource,61:haskellSource,62:javaSource,63:javaScriptSource,64:luaSource,65:ocamlSource,66:octaveSource,67:pascalSource,68:phpSource,43:plainTextSource,69:prologSource,70:pythonSource,71:pythonSource,72:rubySource,73:rustSource,74:typescriptSource,75:cSource,76:cppSource,77:cobolSource,78:kotlinSource,79:objectiveCSource,80:rSource,81:scalaSource,82:sqliteSource,83:swiftSource,84:vbSource,85:perlSource,86:clojureSource,87:fsharpSource,88:groovySource,1001:cSource,1002:cppSource,1003:c3Source,1004:javaSource,1005:javaTestSource,1006:mpiccSource,1007:mpicxxSource,1008:mpipySource,1009:nimSource,1010:pythonForMlSource,1011:bosqueSource,1012:cppTestSource,1013:cSource,1014:cppSource,1015:cppTestSource,1021:csharpSource,1022:csharpSource,1023:csharpTestSource,1024:fsharpSource};var fileNames={45:"main.asm",46:"script.sh",47:"main.bas",48:"main.c",49:"main.c",50:"main.c",51:"Main.cs",52:"main.cpp",53:"main.cpp",54:"main.cpp",55:"script.lisp",56:"main.d",57:"script.exs",58:"main.erl",44:"a.out",59:"main.f90",60:"main.go",61:"main.hs",62:"Main.java",63:"script.js",64:"script.lua",65:"main.ml",66:"script.m",67:"main.pas",68:"script.php",43:"text.txt",69:"main.pro",70:"script.py",71:"script.py",72:"script.rb",73:"main.rs",74:"script.ts",75:"main.c",76:"main.cpp",77:"main.cob",78:"Main.kt",79:"main.m",80:"script.r",81:"Main.scala",82:"script.sql",83:"Main.swift",84:"Main.vb",85:"script.pl",86:"main.clj",87:"script.fsx",88:"script.groovy",1001:"main.c",1002:"main.cpp",1003:"main.c3",1004:"Main.java",1005:"MainTest.java",1006:"main.c",1007:"main.cpp",1008:"script.py",1009:"main.nim",1010:"script.py",1011:"main.bsq",1012:"main.cpp",1013:"main.c",1014:"main.cpp",1015:"main.cpp",1021:"Main.cs",1022:"Main.cs",1023:"Test.cs",1024:"script.fsx"};var languageIdTable={1001:1,1002:2,1003:3,1004:4,1005:5,1006:6,1007:7,1008:8,1009:9,1010:10,1011:11,1012:12,1013:13,1014:14,1015:15,1021:21,1022:22,1023:23,1024:24}
var extraApiUrl="https://extra-ce.Code-Pad.me.com";var languageApiUrlTable={1001:extraApiUrl,1002:extraApiUrl,1003:extraApiUrl,1004:extraApiUrl,1005:extraApiUrl,1006:extraApiUrl,1007:extraApiUrl,1008:extraApiUrl,1009:extraApiUrl,1010:extraApiUrl,1011:extraApiUrl,1012:extraApiUrl,1013:extraApiUrl,1014:extraApiUrl,1015:extraApiUrl,1021:extraApiUrl,1022:extraApiUrl,1023:extraApiUrl,1024:extraApiUrl}