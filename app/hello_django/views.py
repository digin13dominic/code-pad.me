from django.http.response import HttpResponseRedirect, JsonResponse
from django.shortcuts import redirect, render
from django.db.models import Sum

from .models import CodeBase
import humanhash
import os
import binascii
from json import dumps
from random import randint


def index(request):
    if request.method == 'POST':
        if request.POST.get('idurl'):
            pgmlang = request.POST.get('pgmlang', None).strip()
            mycode = request.POST.get('mycode', None).strip()
            stdin = request.POST.get('stdin', None).strip()
            cmdargs = request.POST.get('cmdargs', None).strip()
            description = request.POST.get('description', None).strip()
            expected = request.POST.get('expected', None).strip()
            idurl = request.POST.get('idurl', None).strip()
            challenge = request.POST.get('challenge', None) == 'on'
            customlink = request.POST.get('customlink', None).strip()

            if idurl:
                theone = CodeBase.objects.get(urlhash=idurl)

                if theone.pgmlang == pgmlang and theone.cmdargs ==cmdargs and theone.stdin == stdin and theone.mycode == mycode and theone.description == description and theone.expected == expected and theone.challenge == challenge and theone.customlink == customlink:
                    return HttpResponseRedirect("/"+idurl+"?share=true")
                else:                        
                    hash = binascii.hexlify(os.urandom(16))
                    hash = hash.decode("utf-8")
                    urlhash = humanhash.humanize(hash)
                    if idurl:
                        CodeBase.objects.create(pgmlang=pgmlang,mycode=mycode,stdin=stdin,cmdargs=cmdargs,description=description,expected=expected,urlhash=urlhash,challenge=challenge,customlink=customlink)
                        slug = CodeBase.objects.get(urlhash=urlhash)
                        return HttpResponseRedirect("/"+urlhash+"?share=true")
        else:
            pgmlang = request.POST.get('pgmlang', None).strip() # getting data from first_name input 
            mycode = request.POST.get('mycode', None).strip()
            stdin = request.POST.get('stdin', None).strip()
            cmdargs = request.POST.get('cmdargs', None).strip()
            description = request.POST.get('description', None).strip()
            expected = request.POST.get('expected', None).strip()
            challenge = request.POST.get('challenge', None) == 'on'
            customlink = request.POST.get('customlink', None).strip()
            hash = binascii.hexlify(os.urandom(16))
            hash = hash.decode("utf-8")
            urlhash = humanhash.humanize(hash)

            if urlhash:
                CodeBase.objects.create(pgmlang=pgmlang,mycode=mycode,stdin=stdin,cmdargs=cmdargs,description=description,expected=expected,urlhash=urlhash,challenge=challenge,customlink=customlink)
                slug = CodeBase.objects.get(urlhash=urlhash)
                return HttpResponseRedirect("/"+urlhash+"?share=true")
    else:

        total = CodeBase.objects.all().aggregate(sum=Sum('metrics'))['sum']

        count = CodeBase.objects.filter(challenge=True).count()
        random_object = None
        if count:
            random_object = CodeBase.objects.filter(challenge=True)[randint(0, count - 1)]

        return render(request, 'index.html', {'total' : total,'random_object': random_object })

def detail_view(request, url):


    count = CodeBase.objects.filter(challenge=True).exclude(urlhash=url).count()
    random_object = None
    if count:
        random_object = CodeBase.objects.filter(challenge=True).exclude(urlhash=url)[randint(0, count - 1)]


    theone = CodeBase.objects.get(urlhash=url)
    theone.metrics = theone.metrics+1
    theone.save()
    return render(request, 'index.html', {'theone': theone, 'random_object': random_object , 'domain': 'https://code-pad.me/'})