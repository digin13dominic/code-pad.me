from django.db import models
from django.core.exceptions import ValidationError

def DOMAIN_REGEX(value):
    if "code-pad.me" in value:
        return value
    else:
        raise ValidationError("This field accepts links from code-pad.me only.")    

class CodeBase(models.Model):
    pgmlang = models.CharField(max_length=100)
    mycode = models.CharField(max_length=30000, null=True)
    stdin = models.CharField(max_length=100, null=True)
    cmdargs = models.CharField(max_length=1000, null=True)
    stdout = models.CharField(max_length=1000, null=True)
    description = models.CharField(max_length=1000, null=True)
    expected = models.CharField(max_length=1000, null=True)
    urlhash = models.CharField(max_length=300, null=True)
    metrics = models.IntegerField(null=True,default=1)
    challenge = models.BooleanField(null=True,default=False)
    customlink = models.CharField(max_length=1000, null=True, validators=[DOMAIN_REGEX])
