# Generated by Django 3.2.4 on 2021-08-19 10:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hello_django', '0008_codebase_challenge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='codebase',
            name='challenge',
            field=models.BooleanField(default=False, null=True),
        ),
    ]
